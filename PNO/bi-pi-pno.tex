\documentclass[11pt]{article}

\usepackage{polyglossia}
\setmainlanguage{czech}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{url}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{xevlna}
\usepackage{algpseudocode}
\usepackage{amsfonts}
\usepackage{xcolor}
\usepackage{geometry}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{verbatim}
\titleformat{\section}[block]{\Large\bfseries}{}{1em}{}
\titleformat{\subsection}[hang]{\bfseries\filcenter}{}{1em}{}

\geometry{
  a4paper,
  left=20mm,
  top=25mm,
  right=20mm
}

\setlength{\parindent}{0em}
\setlength{\parskip}{0.7em}

\title{BI-PI PNO 2022}
\author{\small Autor: Vít Mašek}
\date{\small \url{masekvit@fit.cvut.cz}}


\begin{document}

\newcommand{\red}[1]{\textcolor{red}{#1}}
\newtheorem{definition}{Definice}

\maketitle

\tableofcontents

\newpage

\definecolor{vhdlgreen}{RGB}{0,100,0}
\lstnewenvironment{VHDLcode}
  {\lstset{
    language         = VHDL,
    basicstyle       = \ttfamily,
    keywordstyle     = \bfseries,
    identifierstyle  = \color{blue},
    commentstyle     = \color{vhdlgreen},
    stringstyle      = \color{magenta},
    showstringspaces = false,
    columns          = flexible}
  }{}
    
\section{Základní pojmy jazyka VHDL (entita, architektura, proces, signál); syntetizovatelný popis kombinační logiky a sekvenčních obvodů v jazyce VHDL.}
    
\subsection*{Entita}
Popisuje \textbf{pouze} rozhraní daného bloku. Nepopisuje jeho chování nebo vnitřní strukturu. Rozhraní je definováno pomocí portů. Entitu lze parametrizovat pomocí genericů.

\begin{minipage}{\linewidth}
\begin{VHDLcode}
    -- declaration of multiplexer of variable size of inputs
    entity generic_multiplexer is
        generic (
            G_WIDTH : natural := 32
        );
        port (
            a, b    : in    std_logic_vector ( G_WIDTH-1 downto 0 );
            sel     : in    std_logic; 

            y       : out   std_logic_vector ( G_WIDTH-1 downto 0 )
        );
    end entity generic_multiplexer;
\end{VHDLcode}
\end{minipage}

\subsection*{Architectura}

Popisuje chování nebo vnitřní strukturu entity. Musí být asociavaná s konkrétní entitou. Jedna entita může mít více archytektur. Archytektura je paralelní prostředí. Veškeré procesy, paralelní přiřazení a procesy v rámci archytektur instancovaných komponent se vykonávají paralelně.

V archytektuře používáme konstrukty:
\begin{itemize}
    \item proces -- sekvenční kus kódu. Komunikují spolu pomocí signálů.
    \item komponenta -- reprezentuje instanci entity která je součástí dané archytektury.
    \item paralelní přiřazení -- popisuje dataflow v rámci archytektury.  
\end{itemize}

Archytektura má 2 části:
\begin{itemize}
    \item deklarativní -- deklaruje signály, komponenty, definuje konstanty a datové typy.
    \item exekutivní -- obsahuje procesy a paralelní přiřazení, instancuje komponenty. 
\end{itemize}

\begin{VHDLcode}
    architecture rtl of generic_multiplexer is
        -- declarative part:
        signal sel_n, a_sel, b_sel : std_logic;

    begin
        -- executive part:
        sel_n <= not( sel );
        -- ...
    end architecture rtl;
\end{VHDLcode}

\subsection*{Signály}

Signály se používají pro komunikaci mezi procesy, případně instancemi komponent. Signál si udržuje svou hodnotu dokud do něj není znovu přiřazeno pomocí \texttt{<=}. Všechna přiřazení se vykonávají ve stejný čas. Signály mohou mít atributy, jako například \texttt{'event, 'rising\_edge}. Přiřazení do signálu je vždy předpočítáno a naplánováno. Hodnota signálu se doopravdy změní až po době $delta$, kde $delta \rightarrow 0$. Případně lze tuto dobu modifikovat pomocí \texttt{after}.

\subsection*{if, case, loop}

Statement \texttt{if} se svým chování nijak nelyší od ostatních jazyků. Syntaxe je následující:
\begin{multicols}{2}

\begin{VHDLcode}
    if a = '1'' then
        y <= z;
        else
            if b = '1' then
                y <= w;
            else
                y <= u;
        end if;
    end if;
\end{VHDLcode}

\begin{VHDLcode}
    if a = '1' then
        y <= z;
    elsif b = '1' then
        y <= w;
    else
        y <= u;
    end if;
\end{VHDLcode}

\end{multicols}

Statement \texttt{case} musí být definovaný pro všechny možné hodnoty řídícího signálu: 

\begin{VHDLcode}
    type weekday_t is (
        monday, tuesday, wednesday, thursday,
        friday, saturday, sunday );
    signal weekday : weekday_t;
    ...
    case weekday is
        when monday         => -- code
        when tuesday        => --
        when wednesday      => --
        when thursday       => --
        when friday         => --
        when others         => --
    end case;
\end{VHDLcode}

Statement \texttt{loop} se používá pro popis cyklů. 

\pagebreak

\subsection*{Proces}

Proces je sekvenční prostředí v rámci exekutivní čísti archytektury. Jednotlivé prosy mezi sebou komunikují pomocí signálů. Proces sám je vykonáván v nekonečné smyčce. Můžeme ho deklarovat se sensitivity listem -- proces se pak vykoná pokaždé, když nějaký ze signálů v senzitivity listu změní svou hodnotu (nesmí obsahovat wait statement), nebo bez něj -- v tom případě musí proces obsahovat alespoň jeden wait statement.

Následující 2 procesy jsou co se týče chování ekvivalentní.

\begin{multicols}{2}

\begin{VHDLcode}
    mux : process
    begin
        if sel = '0' then
            y <= a;
        else
            y <= b;
        end if;
        wait on sel, a, b;
    end process mux;
\end{VHDLcode}

\begin{VHDLcode}
    mux : process ( sel, a, b )
    begin
        if sel = '0' then
            y <= a;
        else
            y <= b;
        end if;

    end process mux;
\end{VHDLcode}

\end{multicols}

Procesy bez senzitivity listu nejsou synthetizovatelné! Jsou typicky používané v pro testování a popis obvodu na behaviorálním levelu.

\subsection*{Popis kombinační logiky/procesu}

Při popisování kombinační logiky, výstup závisí na hodnotách všech vstupů. Proces popisující kombinační logiku tak musí ve svém sensitivity listu obsahovat všechn vstupy -- řídící signály (\texttt{sel}) a signály které se objevují na pravé straně přiřazení. Výstup musí být definován pro všechny kombinace hodnot vstupních signálů. Zjednodušeně: statement \texttt{if} musí vždy obsahovat \texttt{else} a statement \texttt{case} musí vždy obsahovat větev \texttt{others}. Případně musí být jinak zařízeno, že do výstupního signálu bude vždy přiřazeno, jako například zde:

\begin{VHDLcode}
    mux : process ( sel, a, b )
    begin
        y <= a;
        if sel = '1' then
            y <= b;
        end if;
    end process mux;
\end{VHDLcode}

\pagebreak

\subsection*{Popis sekvenční logiky/procesu}

Popis se stane sekvenčním, pokud nějaký ze vstupních signálů chybí v sensitivity listu nebo pokud při daném průchodu procesem není do výstupního signálu přiřazeno. Tento popis se používá k popisu latchů nebo flip-flopů.

\begin{multicols}{2}

\begin{VHDLcode}
    -- asynchronous reset
    dff : process ( clk )
    begin
        if rst = '1' then
            q <= '0';
        elsif clk = '1' and clk'event then
            q <= d;
        end if;
    end process dff;
\end{VHDLcode}

\begin{VHDLcode}
    -- latch
    latch : process ( en, d )
    begin
        if en = '1' then
            q <= d;
        end if;
    end process latch;
\end{VHDLcode}

\end{multicols}

\begin{multicols}{2}

\begin{VHDLcode}
    -- synchronous reset
    dff : process ( clk )
    begin
        if clk = '1' and clk'event then
            if rst = '1' then
                q <= '0';
            else
                q <= d;
            end if;
        end if;
    end process dff;
\end{VHDLcode}

\begin{VHDLcode}
    -- dff with clock enable
    dff_ce : process ( clk )
    begin
        if clk = '1' and clk'event then
            if rst = '1' then
                q <= '0';
            elsif ce = '1' then
                q <= d;
            end if;
        end if;
    end process dff_ce;
\end{VHDLcode}

\end{multicols}

\end{document}