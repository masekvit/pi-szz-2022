\documentclass[11pt]{article}

\usepackage{polyglossia}
\setmainlanguage{czech}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{url}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{xevlna}
\usepackage{algpseudocode}
\usepackage{amsfonts}
\usepackage[table, dvipsnames]{xcolor}
\usepackage{geometry}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{hyperref}
\hypersetup{colorlinks=true,linkcolor=black, urlcolor=blue}
\usepackage{verbatim}
\titleformat{\section}[block]{\Large\bfseries}{}{1em}{}
\titleformat{\subsection}[hang]{\bfseries\filcenter}{}{1em}{}

\geometry{
  a4paper,
  left=20mm,
  top=25mm,
  right=20mm
}

\setlength{\parindent}{0em}
\setlength{\parskip}{0.7em}

\title{BI-PI ZRS 2022}
\author{Vít Mašek}

\begin{document}

\newcommand{\red}[1]{\textcolor{red}{#1}}
\renewcommand{\b}[1]{\textbf{#1}}
\renewcommand{\i}[1]{\textit{#1}}
\newcommand{\note}[1]{\i{\textcolor{gray}{#1}}}

\maketitle

\tableofcontents

\newpage

\i{\b{=== Pro kontex ===}}

\i{Transformace je změna v matematickém popisu fyzikální proměnné, která má usnadnit výpočet. Transformuje $f (t)$ - funkci v časové oblasti (v tzv. $t$-oblasti) na $F(s)$ - funkci komplexní frekvence $s$ (v tzv. $s$-oblasti). \b{Laplaceova transformace} převádí funkce reálné proměnné na funkce komplexní proměnné způsobem, při němž se mnohé složité vztahy mezi původními funkcemi radikálně zjednoduší.}
  
\i{\b{Laplaceova transformace} se nejčastěji používá k řešení některých diferenciálních rovnic. Je definována vztahem}
$$F(s) = L\{f(t)\} = \int_{0}^{\inf} e^{-st} f(t) dt,$$
\i{kde operátor $s = \sigma+j \omega$.}

\i{\b{Inverzní Laplaceova transformace} je dána vztahem}
$$f(t) = L^{-1}\{F(s)\} = \frac{1}{2\pi j} \int_{\sigma-j \inf}^{\sigma + j \inf} F(s) e^{st} ds.$$

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{Lapleac.png}
\end{figure}
    
\newpage

\section{Vnější popis lineárních spojitých dynamických systémů, přenos, sériová, paralelní a zpětná vazba, metody výpočtu přenosu složitých systémů.}

\subsection*{Vnější popis systémů -- obecně}

Vnější popis systému je vyjádřením dynamických vlastností systému relací mezi jeho vstupem $u(t)$ a výstupem $y(t)$. Nebere se v úvahu vnitřní stav systému $x(t)$.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{popis1.png}
\end{figure}

\subsubsection*{1. Definice systému pomocí množiny vnějších veličin -- zdrojový systém}

Systém je potom definován množinou veličin uvažovaných na určité rozlišovací úrovni, tedy množinou veličin (proměnných) množinami možných hodnot uvažovaných veličin.

Příkladem takového popisu je např. popis chování nádrže s kapalinou, v níž je regulována výška h hladiny kapaliny prostřednictvím řízení napětí na čerpadle a tedy průtokového množství přítoku Q1. Poruchovou veličinou v systému je průtokové množství odtoku Q2 u dna nádrže.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{zdrojovysystem.png}
\end{figure}

\subsubsection*{2. Definice systému pomocí jeho chování -- generativní systém}

Systém je dán časově invariantním vztahem mezi okamžitými, minulými nebo budoucími hodnotami veličin (např. popis chování systému soustavou diferenciálních rovnic).

Příkladem takového popisu je například popis ss motoru s cizím buzením.

\subsubsection*{3. Definice systému pomocí prvků a vazeb mezi nimi}

Systém je dán množinou subsystémů spolu s jejich chováním a množinou vazeb mezi těmito prvky a mezi prvky a jejich okolím.

Příkladem je systém se subsystémy $S1$ a $S2$, které jsou spojené paralelní vazbou.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{popis3.png}
\end{figure}

\subsubsection*{4. Definice systému pomocí stavově přechodové struktury}

Systém je definován množinou stavů spolu s množinou přechodů mezi stavy (stavový diagram).

Příkladem tohoto typu popisu může být např. stavový diagram pro automat na přípravu kávy.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{popis4.png}
\end{figure}

\newpage

\subsection*{Vnější popis systémů -- lineární stacionární dynamické SISO systémy}

\b{SISO} -- Single Input Single Output systém.

\subsubsection*{Možný popis takových sistémů:}
\begin{enumerate}
  \item diferencální rovnicí
  \item přenosovou funkcí v Laplaceově transformaci
  \item impulstní charakteristikou
  \item přechodovou charakteristikou
  \item frekvenčním přenosem
  \item frekvenční charakteristikou
\end{enumerate}

\subsubsection*{1. Vnější popis systému diferenciální rovnicí}

Chování spojitého systému jako řešení diferenciální rovnice s různými počátečními podmínkami je základním způsobem určování chování systému. V řadě praktických případů lze předpokládat, že je-li vstupem systému řídicí veličina, je systém do zahájení řízení bez energie a tedy, že odpovídající diferenciální rovnice má nulové počáteční podmínky.

Spojitý stacionární lineární SISO systém bez dopravního zpoždění ($T_d = 0$, tzn. že výstup
systému reaguje okamžitě na vstup) je popsán obyčejnou diferenciální rovnicí ve tvaru:
$$a_n y^{(n)}(t) + ... + a_0 y(t) = b_m u^{(m)}(t) + ... + b_0 u(t),$$
kde:
\begin{itemize}
  \item $a_i, b_i$ jsou konstantní koeficienty.
  \item $u(t)$ resp. $y(t)$ je vstupní resp. výstupní veličina.
  \item $n$ resp. $n-m$ je řád resp. relativní řád systému.
  \item $(n)$ resp. $(m)$ je $n$-tá resp $m$-tá derivace.
\end{itemize}

Aby byl systém fyzikálně realizovatelný, musí platit \b{podmínka fyzikální realizovatelnosti}:
\begin{itemize}
  \item slabá: $n \geq m$
  \item silná: $n > m$
\end{itemize}

\note{Podmínka fyzikální realizovatelnosti je podmínkou kauzality. Kdyby byl řád pravé strany diferenciální rovnice vyšší než řád strany levé, vznikala by odezva na budicí signál dříve nežli samotný budicí signál začne působit na systém. To je u reálných fyzikálních systémů nemožné!}

\subsubsection*{2. Přenosová funkce systému v Laplaceově transformaci}

\b{Přenosová funkce} $G(s)$ je definována jako poměr Laplaceova obrazu výstupní proměnné systému $Y(s)$ ku Laplaceově obrazu vstupní proměnné systému $U(s)$ za předpokladu, že všechny počáteční podmínky jsou nulové. Je funkcí komplexní proměnné $s$, která popisuje dynamiku systému v komplexní rovině $s$ \textcolor{gray}{\i{(zatímco diferenciální rovnice popisuje dynamiku systému v časové oblasti $t$)}}.

Vychází z popisu diferenciální rovnici, na kterou je aplikována Laplaceova transformace. Po všech možných úpravách dostaneme rovnici
$$Y(s)\left[ a_n s^n + a_{n-1} s^{n-1} + ... + a_0 \right] = U(s)\left[ b_m s^m + b_{m-1} s^{m-1} + ... + b_0 \right]$$

Za nulových počátečních podmínek platí:
$$G(s) = \frac{Y(s)}{U(s)} = \frac{b_m s^m + b_{m-1} s^{m-1} + ... + b_0}{a_n s^n + a_{n-1} s^{n-1} + ... + a_0} = \frac{b(s)}{a(s)}$$

Polynom $a(s)$ nazýváme \b{charakteristickým polynomem}.

\note{
  Příklad: Zjistěte přenosovou funkci G(s) systému, který je popsán diferenciální rovnicí:
  $$5y^{'''}(t) + 2y^{''}(t) + 7 y^{'}(t) + y(t) = u^{''}(t) + 3 u(t)$$
  s nulovými počátečními podmínkami.\\
  Provedeme Laplaceovu transformaci a vyjde nám:
  $$Y(s)[5s^3 + 2s^2 + 7s + 1] = U(s)[s^2 + 3]$$
  Přenosovou funkci $G(s)$ tedy vyjádříme jako podíl výstupní proměnné ku vstupní proměnné:
  $$G(s) = \frac{Y(s)}{U(s)} = \frac{s^2 + 3}{5s^3 + 2s^2 + 7s + 1}$$
  \b{Řád systému} je stupeň charakterystického polynomu $a(s)$.\\
  \b{Přenosové póly} jsou kořeny charakteristického polynomu $a(s)$.\\
  \b{Přenosové nuly} jsou kořeny polynomu $b(s)$.\\
  Systém je fyzickyu realizovatelný, protože $n > m$.\\
  Pokud by $a(s)$ měl nulový kořen, nazveme systém \b{astatický}.\\
  Násobnost takového kořene je násobnost astatismu.
}

\subsubsection*{3. Impulsní charakteristika}

Impulsní charakteristika systému graficky znázorňuje impulsní funkci $g(t)$, která je odezvou systému na Diracův impuls $\delta(t)$ při nulových počátečních podmínkách. Platí:
$$g(t) = L^{-1}\{G(s)\}$$

\subsubsection*{4. Přechodová charakteristika}

Přechodová charakteristika systému graficky znázorňuje přechodovou funkci $h(t)$, která je odezvou systému na jednotkový skok $1(t)$ při nulových počátečních podmínkách. Platí:
$$h(t) =  L^{-1}\left\{\frac{1}{s}G(s)\right\}$$

\subsubsection*{5. Frekvenční přenos}

Frekvenční přenos $G(j\omega)$ je definován jako podíl Fourierova obrazu výstupní veličiny ku Fourierově obrazu vstupní veličiny systému za nulových počátečních podmínek. Protože Laplaceův operátor $s$ odpovídá Furierově operátoru $j\omega$, formálně získáme frekvenční přenos prostou náhradou operátoru $s$ v přenosu v Laplaceově transformaci.

\subsubsection*{6. Frekvenční charakteristika}

Frekvenční charakteristika je grafickým vyjádřením frekvenčního přenosu, tedy amplitudy a fáze ustálené vynucené výstupní veličiny systému v závislosti na frekvenci harmonického vstupu.

\subsection*{Paralelní, sériová a zpětná vazba}

\subsubsection*{Paralelní vazba}

Chceme-li systémy $S1$ a $S2$ spojit paralelně, musí mít oba systémy stejný počet vstupů a stejný počet výstupů.
\begin{itemize}
  \item Vstup $u$ je vstupem všech subsystémů -- $u = u_1 = u_2$.
  \item Výstup paralelního spojení $y$ je součtem výstupů všech subsystémů -- $y = \sum_{i \in \hat{i}} y_i$.
  \item Přenos $G(s)$ celého systému je součtem přenosů všech subsystémů -- $G(s) = \sum_{i \in \hat{i}} G_i(s)$.
\end{itemize}
  
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{paralelnivazba.png}
\end{figure}

\subsubsection*{Sériová vazba}

Abychom subsystémy $S1$ a $S2$ mohli spojit sériově, musí mít subsystém $S1$ tolik výstupů, kolik má subsystém $S2$ vstupů.
\begin{itemize}
  \item Vstup $u$ je vstupem do prvního subsystému -- $u = u_1$.
  \item Výstup $y$ je výstupem posledního (zde 2.) subsystému -- $y = y_2$.
  \item Přenos $G(s)$ celého systému je součinem přenosů všech subsystémů -- $G(s) = \prod_{i \in \hat{i}}G_i(s)$.
\end{itemize}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{seriovavazba.png}
\end{figure}

\subsubsection*{Zpětná (antiparalelní) vazba}

Abychom subsystémy $S1$ a $S2$ mohli spojit antiparalelně, musí mít subsystém $S1$ tolik výstupů, kolik má subsystém $S2$ vstupů a subsystém $S2$ tolik výstupů, kolik má subsystém $S1$ vstupů. Platí:
\begin{itemize}
  \item $u_1 = u \pm y_2$
  \item $y = y_1 = u_2$
\end{itemize}

Přenosová funkce $G(s)$ složeného systému $S$ se \b{zápornou} zpětnou vazbou:
$$G(s) = \frac{G_1(s)}{1 + G_1(s) G_2(s)}$$

Přenosová funkce $G(s)$ složeného systému $S$ s \b{kladnou} zpětnou vazbou:
$$G(s) = \frac{G_1(s)}{1 - G_1(s) G_2(s)}$$

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{zpetnavazba.png}
\end{figure}

\subsection*{Zjednodušování blokových schémat složitých systémů}

Pokud je systém složitý a složený z mnohu subsystémů které jsou spojeny paralelní, sériovou a zpětnou vazbou, vypočítáme celkový přenos tohoto systému prostupným uplatňováním vzorců pro přenos a slučováním jednotlivých subsystémů tak, že nakonci vznikne jeden velký.

\begin{itemize}
  \item Metoda postupného zjednodušování blokového schématu
  \item Metoda postupného vylučování mezilehlých proměnných
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\section{PID regulátor, popis PID regulátoru a jeho dynamické vlastnosti, PID regulátory s interakcí a bez interakce, vliv nastavení PID regulátoru na stabilitu regulačního obvodu.}

\subsection*{Regulační obvod -- obecně}

Regulační obvod vzniká zpětnovazebním připojením regulátoru k regulované soustavě. Regulátory se konstruují proto, aby řízení procesu nevyžadovalo nepřetržitou pozornost a ruční zásahy operátora. Regulátor automaticky mění akční veličinu $y_R(t)$ tak, aby regulovaná veličina $y(t)$ měla žádanou hodnotu $w(t)$.

Činnost regulačního obvodu je založena na neustálém srovnávání vstupu $w(t)$ , který vyjadřuje požadované chování, s výstupem $y(t)$, který podává informaci o skutečném chování. Na vstupu regulátoru $R$ vzniká regulační odchylka $e(t) = w(t) - y(t)$, která je v regulátoru funkčně upravena a jako řídicí veličina $u(t)$ vstupuje do regulované soustavy $S$. Cílem je, aby regulační odchylka $e(t)$ byla co nejmenší.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{regulator.png}
  \caption{Obecný pohled na regulátor}
\end{figure}

\subsection*{PID regulátor (Proporcionálně -- Integračně -- Derivační)}

Z fyzikálního hlediska se požadovaného chování řízeného systému dosahuje zpracováním informace obsažené v odchylce $e(t)$ pomocí \b{Proporcionálních}, \b{Integračních} a \b{Derivačních} členů ve zpětné vazbě, označovaných jako PID regulátor. Podíl těchto složek se nastavuje pomocí parametrů regulátoru (P $\rightarrow r_0$, I $\rightarrow r_{-1}$, D $\rightarrow r_1$)

\subsubsection*{Proporcionální člen -- P}

Značí se $r_0$ a označuje proporcionální konstantu, neboli zesílení PID regulátoru.
$$y_P(t) = r_0 e(t)$$
$$G_P(s) = r_0$$

\subsubsection*{Integrační člen -- I}

Určuje reakci regulátoru na dobu trvání regulační odchylky. Integrační konstantu značíme $r_{-1}$. Při integračním chování je akční zásah úměrný době $\tau$, po kterou existuje regulační odchylka.
$$y_I(t) = r_{-1} \int_{0}^{t}e(\tau) d\tau$$
$$G_I(s) = \frac{r_{-1}}{s}$$

\subsubsection*{Derivační člen -- D}

Je determinujícím faktorem reakce regulátoru na rychlost změny regulační odchylky. Derivační konstantu značíme $r_0$.
$$y_D(t) = r_1 e^{'}(t)$$
$$G_D(s) = r_1 s$$

\subsubsection*{Integrační časová konstanta}
$$T_i = \frac{r_0}{r_{-1}}[s]$$
Znázorníme-li odezvu PI regulátoru na jednotkový skok $e(t) = 1(t)$, můžeme $T_i[s]$ interpretovat jako čas, který by potřeboval čistě I regulátor, aby přestavil akční člen do polohy, které dosáhne PI regulátor v čase $t=0$ vlivem své P složky.

\subsubsection*{Derivační časová konstanta}
$$T_d = \frac{r_1}{r_0}[s]$$
Znázorníme-li odezvu PD regulátoru na jdnotkový skok $e(t) = t$, můžeme $T_d[s]$ interpretovat jako čas, který by potřeboval čistě P regulátor, aby přestavil akční člen do polohy, které dosáhne PD regulátor v čase $t=0$ vlivem své D složky.

\subsubsection*{Přenos PID regulátoru}

Za předpokladu nulových počátečních podmínek má přenos ideálního PID regulátoru po Laplaceově transformaci tvar
$$G_R(s) = r_0 +\frac{r_{-1}}{s} + r_1s = r_0 \left[ 1 + \frac{1}{T_i s} + T_d s\right]$$

\subsection*{PID regulátory s interakcí}

PID regulátory, u kterých změna velikosti jedné konstanty způsobuje přenastavení některé popř. všech konstant zbývajících, nazýváme regulátory s interakcí. Takovým regulátorem může být například sériové zapojení jednotlivých členů.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.5\textwidth]{PIDserie.png}
  \caption{PID regulátor s interakcí realizovaný sériovým řazením členů P, PD a PI}
\end{figure}

\b{Regulátory s interakcí 1. třídy} jsou takové, pro něž platí, že poměr \b{integrační} a \b{derivační} časové konstanty je omezen na $\left\langle 0, 1/4\right\rangle$.

\b{Regulárory s interakcí 2. třídy} jsou omezeny dvojnásobně, tedy poměr \b{integrační} a \b{derivační} časové konstanty je omezen na $\left\langle 0, 1/8\right\rangle$.

\subsubsection*{Regulátor bez interakce:}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.4\textwidth]{PIDbezinter.png}
  \caption{PID regulátor bez interakce}
\end{figure}

\newpage
\subsection*{Vliv nastavení PID regulátoru na stabilitu systému}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{grafik.png}
\end{figure}

\begin{itemize}
  \item Doba náběhu $t_n$ je čas potřebný pro zvýšení úrovně signálu z 10 \% na 90 \% ustálené hodnoty.
  \item $y_{Rmax}$ je maximální překývnutí regulované veličiny v čase $t_m$.
  \item $T$ je perioda kmitů.
  \item $t_r$ je doba regulace.
\end{itemize}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{vlivparametru.png}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Regulační obvod s číslicovým (PSD) regulátorem, schéma, diskrétní náhrady integrace a derivace, absolutní a přírůstkový algoritmus a přenos PSD regulátoru.}

\subsection*{Diskrétní regulační obvod}

Analogové regulátory jejichž chování je popsáno pomocí diferenciálních rovnic nebo přenosů v Laplaceově transformaci a realizováno prostřednictvím prvků analogové elektroniky, je možné aproximovat pomocí diskrétních regulátorů. Ty jsou popsány diferenčními rovnicemi a realizovány pomocí číslicových počítačů. 

Diskrétní regulační obvod je obvod, ve kterém alespoň jeden člen pracuje diskrétně, tzn. informaci přijímá nebo vydává v diskrétních časových okamžicích, které jsou většinou rovnoměrné. V diskrétním regulačním obvodu je tedy alespoň jedna veličina k dispozici pouze ve tvaru posloupnosti diskrétních hodnot. Ty jsou vzorkovány v pravidelně se opakujících okamžicích označovaných jako perioda $T$.

\subsubsection*{Struktura regulačního obvodu s číslicovým regulátorem}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{strukturacislicregul.png}
  \caption{Struktura ústředního členu regulátoru a jeho začlenění do regulačního obvodu}
\end{figure}

\newpage

\subsection*{PSD regulátor (Proporcionálně -- Sumačně -- Diferenční)}

Číslicové PSD regulátory jsou diskrétní analogií PID regulátorů. Svým chováním se k nim však jen přibližují (PSD je aproximací PID).

Hodnota akční veličiny $y_R(t)$ PSD regulátoru je definována pouze v dikrétních časových okamžicích $t = kT, k = 0,1,2,...$:
$$y_R(kT) =  r_0 \left[ e(kT) + \frac{1}{T_i} I(kT) + T_d D(kT)\right],$$
kde $I(kT)$ resp. $D(kT)$ je hodnota integrálu resp. derivace v diskrétním časovém okamžiku $t=kT$.

Výpočet těchto hodnot se provádí numericky. Většinou se používají přibližné diskrétní náhrady spojitých algoritmů integrace a derivace.

\subsection*{Diskrétní náhrada integrace}

\begin{figure}[hb!]
  \centering
  \includegraphics[width=.7\textwidth]{zpetnaobdelnikova.png}
  \caption{\b{Zpětná obdélníková} náhrada integrace}
\end{figure}

\begin{figure}[hp!]
  \centering
  \includegraphics[width=.7\textwidth]{doprednaobdelnikova.png}
  \caption{\b{Dopředná obdélníková} náhrada integrace}
\end{figure}

\begin{figure}[hp!]
  \centering
  \includegraphics[width=.7\textwidth]{lychobeznikova.png}
  \caption{\b{Lichoběžníková} náhrada integrace}
\end{figure}

\newpage
\subsection*{Diskrétní náhrada derivace}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{nahradaderivace.png}
  \caption{Diskrétní náhrada derivace -- Zpětná diference 1. řádu.}
\end{figure}

\subsection*{Polohový algoritmus}

Vychází z náhrady integrálu ve spojitém PID regulátoru \b{zpětnou obdélníkovou metodou} a náhrady derivace \b{zpětnou diferencí}.

Nemá rekurentní charakter. K výpočtu aktuální hodnoty akční veličiny $y_R(kT)$ je nezbytné uchovávat v paměti všechny hodnoty regulační odchylky $e(iT), i=0,1,2,…k$, což je značně nevýhodné. Tuto nevýhodu odstraňuje přírůstkový PSD algoritmus řízení.

\subsection*{Přírůstkový algoritmus}

Neurčuje v aktuálním diskrétním okamžiku $kT$ okamžitou hodnotu $y_R(kT)$ akční veličiny, ale pouze její (kladný nebo záporný) přírůstek $\Delta y_R(kT)$ vůči $y_R((k-1)T)$.

Při využití \b{zpětné obdélníkové náhrady integrace} dostáváme po všech možných ošklivých úpravách ještě ošklivějšího prvotního vzorečku:
$$\Delta y_R(kT) = q_0 e(kT) + q_1 e((k-1)T) + q_2 e((k-2)T),$$
kde
$$q_0 = r_0 \left(1 + \frac{T}{T_i} + \frac{T_d}{T} \right) \qquad\qquad q_1 = -r_0 \left(1 + 2 \frac{T_d}{T} \right) \qquad\qquad q_2 = r_0 \frac{T_d}{T}$$

Hodnotu $y_R(kT)$ pak můžeme vyjádřit rekurentní rovnicí
$$y_R(kT) = y_R((k-1)T) + \Delta y_R(kT)$$

Přírůstkový algoritmus tedy generuje aktuální hodnotu akční veličiny na základě:
\begin{enumerate}
  \item Její předešlé hodnoty $y_R((k-1)T)$.
  \item Aktuální hodnoty regulační odchylky $e(kT)$.
  \item Minulé a předminulé hodnoty regulační odchylky $e((k-1)T)$ a $e((k-2)T)$.
\end{enumerate}

\note{Za povšimnutí stojí, že hodnota parametů $q_{0,1,2}$ závisí také na vzorkovací periodě $T$.}

Pokud zvolíme jinou náhradu integrace než zpětnou obdélníkovou, algoritmus se liší pouze ve výpočtu parametrů $q_{0,1,2}$.

\subsection*{Přenos diskrétních regulátorů}

\b{Přenos diskrétních regulátorů P a PS:}
$$G_R(z) = \frac{q_0 + q_1 z^{-1}}{1-z^{-1}}$$

\b{Přenos diskrétních regulátorů PD a PSD:}
$$G_R(z) = \frac{q_0 + q_1 z^{-1} + q_2 z^{-2}}{1-z^{-1}}$$

\end{document}