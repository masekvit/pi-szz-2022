Link na stručnější přednášky týkajcích se všech 5 otázek:

https://www.youtube.com/playlist?list=PLAkt7IhxvteIrUlAxpr_S11UaZd601e7X



Jako slidy používat ty ze semestru B201 (kdy se ještě navrhuje MIPS mikroakritektura a ne RISC-V), které se používají i v nahraných přednáškách:

https://courses.fit.cvut.cz/BI-APS/@B201/lectures/index.html